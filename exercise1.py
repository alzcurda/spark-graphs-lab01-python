import pyspark
import os
from pyspark.sql.types import *
from pyspark.sql import SQLContext
from pyspark.sql import Row
from graphframes import *

def warmup(ctx, sqlCtx):
    vertices_list = []
    vertices_list.append(Row("a","Alice", 34))
    vertices_list.append(Row("b","Bob", 36))
    vertices_list.append(Row("c","Charlie", 30))
    vertices_list.append(Row("d","David", 29))
    vertices_list.append(Row("e","Esther", 32))
    vertices_list.append(Row("f","Fanny", 36))
    vertices_list.append(Row("g","Gabby", 60))

    vertices_rdd = ctx.parallelize(vertices_list)

    fields = [StructField("id", StringType(), True),
            StructField("name", StringType(), True),
            StructField("age", IntegerType(), True)]

    vertices_schema = StructType(fields)

    vertices =  sqlCtx.createDataFrame(vertices_rdd, vertices_schema)

    edges_list = []
    edges_list.append(Row("a","b","friend"))
    edges_list.append(Row("b","c","follow"))
    edges_list.append(Row("c","b","follow"))
    edges_list.append(Row("f","c","follow"))
    edges_list.append(Row("e","f","follow"))
    edges_list.append(Row("e","d","friend"))
    edges_list.append(Row("d","a","friend"))
    edges_list.append(Row("a","e","friend"))

    edges_rdd = ctx.parallelize(edges_list)

    fields = [StructField("src", StringType(), True),
            StructField("dst", StringType(), True),
            StructField("relationship", StringType(), True)]

    edges_schema = StructType(fields)
    edges = sqlCtx.createDataFrame(edges_rdd, edges_schema)

    gf = GraphFrame(vertices,edges)
    

    gf.edges.show()
    gf.vertices.show()
