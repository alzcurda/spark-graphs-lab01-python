import os
import sys
import pyspark

from pyspark.sql import SQLContext

import exercise1
import exercise2
import exercise3
import prueba	

HADOOP_HOME = "F:\Git\spark-graphs-lab01-python\hadoop_home"

if(__name__== "__main__"):
	os.environ["HADOOP_HOME"] = HADOOP_HOME
	sys.path.append(HADOOP_HOME + "\\bin")
	conf = pyspark.SparkConf().setAppName("SparkGraphs_I")
	sc = pyspark.SparkContext(conf=conf)
	sc.setLogLevel("ERROR")
	sqlctx = SQLContext(sc)
	if(len(sys.argv) < 2):
		print("You have to provide an exercise number (exercise1, exercise2, exercise3)")
		exit()
	elif(sys.argv[1] == "exercise1"):
			exercise1.warmup(sc, sqlctx)
	elif(sys.argv[1] == 'exercise2'):
			exercise2.basicGraphFrames(sc, sqlctx)
	elif(sys.argv[1] == 'exercise3'):
			exercise3.wikipedia(sc, sqlctx)
	elif(sys.argv[1] == 'prueba'):
			prueba.joinMult(sc, sqlctx)
	else:
			print("Wrong exercise number (exercise1, exercise2, exercise3)")
			exit()