import pyspark
import os
from pyspark.sql.types import *
from pyspark.sql import SQLContext
from pyspark.sql import Row
from pyspark.sql.functions import *
from graphframes import *
#spark-submit --packages graphframes:graphframes:0.8.0-spark3.0-s_2.12 main.py exercise3

def wikipedia(ctx, sqlCtx):        
        fields = [StructField("id", StringType(), True),
                StructField("desc", StringType(), True)]

        vertices_schema = StructType(fields)
        
        vertices = sqlCtx.read.options(delimiter='\t', header='false') \
				.schema(vertices_schema) \
				.csv("./resources/wiki-vertices.txt")        
        
        fields = [StructField("src", StringType(), True),
                StructField("dst", StringType(), True)]

        edges_schema = StructType(fields)

        edges = sqlCtx.read.options(delimiter='\t', header='false') \
				.schema(edges_schema) \
				.csv("./resources/wiki-edges.txt") 
                        
        gf = GraphFrame(vertices,edges)        

        gf.edges.show()

        gf.vertices.show()
        #Calculamos pagerank
        results = gf.pageRank(resetProbability=0.15, maxIter=30)
        #Mostramos los datos de las 10 primeras
        results.vertices.sort(desc("pagerank")).show(10)        

        return