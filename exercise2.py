import pyspark
import os
from pyspark.sql.types import *
from pyspark.sql import SQLContext
from pyspark.sql import Row
from pyspark.sql.functions import *
from graphframes import *

#spark-submit --packages graphframes:graphframes:0.8.0-spark3.0-s_2.12 main.py exercise2

def basicGraphFrames(ctx, sqlCtx):
        #*************** 2.1 Load Graph ******************
        fields = [StructField("id", StringType(), True),
                StructField("name", StringType(), True),
                StructField("age", IntegerType(), True)]

        vertices_schema = StructType(fields)
        
        vertices = sqlCtx.read.options(delimiter=',', header='true') \
				.schema(vertices_schema) \
				.csv("./resources/people.txt")        
        
        fields = [StructField("src", StringType(), True),
                StructField("dst", StringType(), True),
                StructField("likes", IntegerType(), True)]

        edges_schema = StructType(fields)

        edges = sqlCtx.read.options(delimiter=',', header='true') \
				.schema(edges_schema) \
				.csv("./resources/likes.txt") 

        gf = GraphFrame(vertices,edges)        

        gf.edges.show()

        gf.vertices.show()
        
        ex1 = gf.filterVertices("age>30").vertices.collect()

        for r in ex1:        
                print(r.name,'is',r.age)  

        #************** 2.2 Graph Views ******************
        ex2 = gf.triplets.collect()
        for r in ex2:        
                print(r.src.name,'likes',r.dst.name)

        ex2_2 = gf.filterEdges("likes>5").triplets.collect()
        for r in ex2_2:        
                print(r.src.name,'loves',r.dst.name)
                 
        #*********** 2.3 Querying the Graph **************        
        #Youngest
        gf.vertices.groupBy().min("age").show()
   
        likesG = gf.edges.groupBy("src").sum("likes").withColumnRenamed("sum(""likes"")","likesDados")
        likesR = gf.edges.groupBy("dst").sum("likes").withColumnRenamed("sum(""likes"")","likesRecibidos")
        usuarios =  gf.vertices
        likes = likesG.join(likesR, likesG.src == likesR.dst,how="full")\
                .select((coalesce(likesG.src,likesR.dst)).alias("id") 
                ,(coalesce(likesG.likesDados,lit(0))).alias("likesDados")
                ,(coalesce(likesR.likesRecibidos,lit(0))).alias("likesRecibidos")).alias("likes")
        likes.join(usuarios, ['id'], how="full").select(usuarios.name, likes.likesDados, likes.likesRecibidos).show()
    
        # ************* 2.4 Motif Finding *****************
        #Relación A->B->C sin C->A
        gf.find("(a)-[e]->(b); (b)-[e2]->(c); !(c)-[]->(a)").show()
        #Subgrafos con mas de 2 likes
        gf.filterEdges("likes>2").dropIsolatedVertices().triplets.show()
        #Otra forma de hacerlo con find
        gf.find("(a)-[e]->(b)").filter("e.likes>2").show()
        #Añadimos otro ejemplo mas complejo
        gf.find("(a)-[e]->(b); (b)-[e2]->(c)").filter("e.likes+e2.likes>10").show()
        
        return