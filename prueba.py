import pyspark
import os
from pyspark.sql.types import *
from pyspark.sql import SQLContext
from pyspark.sql import Row
from pyspark.sql.functions import *
from graphframes import *

#spark-submit --packages graphframes:graphframes:0.8.0-spark3.0-s_2.12 main.py exercise2

def joinMult(ctx, sqlCtx):
        #*************** 2.1 Load Graph ******************
        fields = [StructField("id", StringType(), True),
                StructField("name", StringType(), True),
                StructField("age", IntegerType(), True)]

        vertices_schema = StructType(fields)
        
        vertices = sqlCtx.read.options(delimiter=',', header='true') \
				.schema(vertices_schema) \
				.csv("./resources/people.txt")                                
        
        fields = [StructField("src", StringType(), True),
                StructField("dst", StringType(), True),
                StructField("likes", IntegerType(), True)]

        edges_schema = StructType(fields)

        edges = sqlCtx.read.options(delimiter=',', header='true') \
				.schema(edges_schema) \
				.csv("./resources/likes.txt") 

        vertices.show()
        edges.show()

        #vertices.join(edges, vertices.id == edges.src,how="inner").alias("jAnt").join(edges, jAnt.id == edges.src,how="inner").show()
        eId = edges.withColumnRenamed("src","id")
        vertices.join(eId, ["id"],how="inner").join(vertices, ["id"],how="inner").show()

        return